Plasmids used for Graham et al., "Proximity-assisted photoactivation (PAPA): Detecting molecular interactions in live-cell single-molecule imaging"

pTG747	EF1alpha-3xFlag-Halo-SNAPf-3xNLS

pTG765	EF1alpha-FRB-Halo-3xNLS

pTG770	EF1alpha-3xFlag-Halo-mAR

pTG771	EF1alpha-SNAPf-mAR

pTG781	EF1alpha-3xFlag-Halo-3xNLS-P2A-T2A-SNAPf-3xNLS

pTG800	3xFlag-Halo-SNAPf-3xNLS-T2A-P2A-H2B-SNAPf-3xNLS

pTG802	3xFlag-Halo-SNAPf-60mer-T2A-P2A-SNAPf

pTG818	EF1alpha-3xFlag-Halo-1xIg-SNAPf-3xNLS PiggyBac Puro

pTG820	EF1alpha-3xFlag-Halo-3xIg-SNAPf-3xNLS PiggyBac Puro

pTG824	EF1alpha-SNAPf-3xNLS-T2A-P2A-H2B-Halo-SNAPf

pTG825	EF1alpha-SNAPf-60mer-T2A-P2A-Halo-SNAPf

pTG828	EF1alpha-3xFlag-Halo-5xIg-SNAPf-3xNLS PiggyBac Puro

pTG838	EF1alpha-H2B-Halo-SNAPf-3xNLS

pTG839	EF1alpha-3xFlag-Halo-SNAPf-60mer

pTG859	EF1alpha-3xFlag-Halo-7xIg-SNAPf-3xNLS PiggyBac Puro
